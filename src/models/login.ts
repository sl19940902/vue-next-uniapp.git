/*
 * @Description:
 * @Author: shuliang
 * @Date: 2022-06-27 15:18:40
 * @LastEditTime: 2022-06-27 16:16:32
 * @LastEditors: shuliang
 */
export interface ILogin {
  userName: string
  photo: string
  time: string
  roles: Array<string>
  authBtnList: Array<string>
}

export interface ILoginParams {
  page: number
  pageSize: number
  [keys: string]: any
}

export interface UserInfosStates {
  userInfos: ILogin
  token: string
}
